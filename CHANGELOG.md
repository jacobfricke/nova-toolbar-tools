## 1.2.2 - 2023-02-25

**Fixes:**
- Fix broken js because of missing `_`
- Fix broken item-display

## 1.2.1 - 2023-02-08
- Autofocus search input
- Fix broken clear button

## 1.2.0 - 2023-02-07
- Add locales
- Make search-input label customizable
- Fix issues with missing search input

## 1.1.0 - 2023-02-07
- Save selection on tool instance
- Use existing tool instance for getting selection thus reducing repeated calls to `Model::find`
- Only fetch selection from session on demand
- Forget `NovaRequest` instance in case it didn't exist before call to `app(NovaRequest::class)` (see #1)

## 1.0.1
- Fix config for conflicting packages

## 1.0.0
- Rework for Nova 4

## 0.5.0
- Add clearable option

## 0.4.0
- Make dropdown searchable
- Add sorting option

## 0.3.0
- Fix bug with response type of boot-middleware
- Add trait for better access to selection

## 0.2.2
- Define the axios dependency extern to reduce js-bundle size
- Fix error while calling the custom change handler

## 0.2.1
- include compiled assets into repository for packagist

## 0.2.0
- Fix problem with selection
- Add ability to manipulate/replace the index-query
