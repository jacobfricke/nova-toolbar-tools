# Requirements

* `php: ^8.0`
* `laravel/nova: ^4.0` *(see version `0.5` for Nova 3)*

# Nova Toolbar Tools

A composer package for [laravel nova](https://nova.laravel.com/). It enables you to globally select and persist resources.

Example use-cases:
- Base for developing your own toolbar-tools
- Select a model you want to filter for in some resources without applying the filter every time but only once every session
  - Customize the menu based on the selection
    - Hide or show resources
    - Hide or show tools
  - Customize tool behaviour based on global selection

## Installation

```
composer require gabelbart/nova-toolbar-tools
```

**Add the middleware to the nova config**

Add `\Gabelbart\Laravel\Nova\ToolbarTools\Http\Middleware\BootToolbarTools::class` to the `middleware` list in `config/nova.php`

**Load tools in your `\App\Providers\NovaServiceProvider`**

Add this method:

```php
public function toolbarTools()
{
    return [
    ];
}
```

Add these lines to the `boot` method:
```php
Nova::serving(function () {
    \Gabelbart\Laravel\Nova\ToolbarTools\ToolbarTools::toolbarTools($this->toolbarTools());
});
```

*If you already have a `Nova::serving` block, you might just combine the contsnts.*

## Usage

### Toolbar resources

#### Set up toolbar resource

Let's say you have a Resource `\App\Nova\Publisher`, to create a toolbar-dropdown you need to create an instance of `\Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarResource`. 
 Add the following to the `toolbarTools` method of your `NovaServiceProvider`:
```php
ToolbarResource::make(\App\Nova\Publisher::class)
```

It might look like this then:
```php
public function toolbarTools()
{
    return [
        ToolbarResource::make(\App\Nova\Publisher::class),
    ];
}
```

#### Access the value

Anywhere in nova you can now access the value with the static `sessionValueFor` method of `\Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarResource`.
 For example:
```php
/** @var ?\App\Models\Publisher $publisher */
$publisher = ToolbarResource::sessionValueFor(\App\Nova\Publisher::class);
```

In addition, you can use the `\Gabelbart\Laravel\Nova\ToolbarTools\Traits\HasToolbarSelection` trait.
 This enables you to call the `getToolbarSelection` and `getToolbarSelectionOrFail` static methods of your resources.

```php
class Publisher extends \Laravel\Nova\Resource
{
    use \Gabelbart\Laravel\Nova\ToolbarTools\Traits\HasToolbarSelection;

    // snip...
}

/** @var ?\App\Models\Publisher $model */
$model = Publisher::getToolbarSelection();
```

#### Options

**Label**

By default the label of your nova-resource will be used, this can be customized:

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->withLabel(__('my_own_label'))
```

**Searchable**

By default all possible options will be displayed.
 If you rather want the user to search you can use the `searchable` option.

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->searchable()
```

*This leverages the global search of nova.*

**Filterable**

By default all possible options will be displayed.
 You can show a filter-field for filtering the values based on text input using the `filterable` option.
 When used together with `searchable` the search option will take precedence.

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->searchable()
```

**Selection**

By default the value from the session will be used, if you turned that off you might want to set the selected value yourself.

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->selection(\Auth::user()->favoritePublisher())
```

**SortBy**

To change the order of the items use the `sortBy` option.
 It accepts three parameters:
 1. **key** `string|callable|false` - name of the property to order by or callback, pass `false` to disable search
 2. **options** `array` (default: `null`) - any option available in Collection::sortBy
 3. **descending** `bool` (default: `false`) - set the sorting direction

The parameters are directly passed to the [`Illuminate\Support\Collection::sortBy`](https://laravel.com/docs/8.x/collections#method-sortby) method, read there for more information.

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->orderBy('name', null, false)
```

**NoItemsFoundText**

With the `searchableDropdown` option, the component will show 'No items found' if there are no matches.
 You can customize this message by using:

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->noItemsFoundText(__('my_no_items_found_text')
```

**Saving to session**

By default the value from the selection will be remembered within the session.
 If you want to change that call the `saveToSession` method with `false` as parameter.
 You might want to consider persisting the value and providing the selection yourself then,
 have a look at `selection` and `onChange`.

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->saveToSession(false)
```

**Reacting to selection**

If you want to customize the behaviour for a selection change you can do this within a custom callback.
 The request will have a parameter called `id` containing the primary key of the selection.

```php
ToolbarResource::make(\App\Nova\Publisher::class)
    ->onChange(fn (\Illuminate\Http\Request $request) => \App\Nova\Publisher::onChangeToolbarSelection($request))
```

**Manipulating the index-query**

*__Note:__ This will __not__ work for the `searchable` option since it's using the global search of nova.*

If you want to customize the list of available options you can use the `withIndexQuery` method.
 It receives the internal query builder as parameter.
 However, you can return your own query builder instance if you want to replace the internal query builder.

```php
ToolbarResource::make(\App\Nova\Book::class)
    ->withIndexQuery(fn (\Illuminate\Database\Eloquent\Builder $builder) => $builder->where('publisher_id', \Auth::user()->id))
```
