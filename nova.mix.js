const path = require('path')

const mix = require('laravel-mix')

// eslint-disable-next-line no-unused-vars
const webpack = require('webpack')

class NovaExtension {
  name () {
    return 'nova-extension'
  }

  register (name) {
    this.name = name
  }

  webpackConfig (webpackConfig) {
    webpackConfig.externals = {
      vue: 'Vue',
    }

    webpackConfig.resolve.alias = {
      ...(webpackConfig.resolve.alias || {}),
      'laravel-nova': path.join(
        __dirname,
        './vendor/laravel/nova/resources/js/mixins/packages.js'
      ),
      'laravel-nova-components': path.join(
        __dirname,
        './vendor/laravel/nova/resources/js/components'
      ),
    }

    webpackConfig.output = {
      uniqueName: this.name,
    }
  }
}

mix.extend('nova', new NovaExtension())
