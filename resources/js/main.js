import ThemeDropdown from './components/Replacements/ThemeDropdown'
import ToolbarResource from './components/ToolbarResource'

Nova.booting((app, store) => {
  app.component('ThemeDropdown', ThemeDropdown)
  app.component('ToolbarResource', ToolbarResource)
})
