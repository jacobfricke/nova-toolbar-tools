<?php

return [
    'search' => 'Suchen...',
    'no_items_found' => 'Keine :label gefunden.',
];
