<?php

return [
    'search' => 'Search...',
    'no_items_found' => 'No :label found.',
];
