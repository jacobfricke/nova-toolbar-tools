<?php

use Illuminate\Support\Facades\Route;

use Gabelbart\Laravel\Nova\ToolbarTools\Http\Middleware\AuthorizeToolbarResource;

Route::prefix('tools')
    ->group(function () {
        Route::get('', [
            \Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers\ToolbarToolsController::class,
            'get'
        ]);
    });

Route::middleware([AuthorizeToolbarResource::class])
    ->prefix('toolbar-resources')
    ->group(function () {
        Route::prefix('{resource}')
            ->group(function () {
                Route::get('', [
                    \Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers\ToolbarResourceController::class, 'get'
                ]);
                Route::post('', [
                    \Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers\ToolbarResourceController::class, 'post'
                ]);
                Route::delete('', [
                    \Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers\ToolbarResourceController::class, 'delete'
                ]);
            });
    });
