<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Http\Controllers;

use Illuminate\Http\Request;

use Gabelbart\Laravel\Nova\ToolbarTools\Tools\ToolbarTool;
use Gabelbart\Laravel\Nova\ToolbarTools\ToolbarTools;

class ToolbarToolsController extends Controller
{
    public function get(Request $request): \Illuminate\Http\JsonResponse
    {
        return response()->json(array_map(
            fn (ToolbarTool $tool) => $tool->jsonSerialize(),
            ToolbarTools::availableToolbarTools($request)
        ));
    }
}
