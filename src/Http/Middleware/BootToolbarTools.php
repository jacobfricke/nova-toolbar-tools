<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Http\Middleware;

use Illuminate\Http\Request;

use Gabelbart\Laravel\Nova\ToolbarTools\ToolbarTools;

class BootToolbarTools
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, $next)
    {
        ToolbarTools::bootToolbarTools($request);

        return $next($request);
    }
}
