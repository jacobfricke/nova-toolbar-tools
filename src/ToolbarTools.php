<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools;

use Illuminate\Support\Facades\Facade;

class ToolbarTools extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return ToolbarToolsService::class;
    }
}
