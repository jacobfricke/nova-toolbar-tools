<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Tools;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;

use Laravel\Nova\GlobalSearch;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Resource;

use Gabelbart\Laravel\Nova\ToolbarTools\ToolbarTools;
use Gabelbart\Laravel\Nova\ToolbarTools\Tools\Traits\UsesNovaRequest;

class ToolbarResource extends ToolbarTool
{
    use UsesNovaRequest;

    public string $component = 'toolbar-resource';
    /** @var class-string<Resource> $resourceClass */
    protected string $resourceClass;
    /** @var ?callable */
    protected $changeCallback = null;
    /** @var ?callable */
    protected $deleteCallback = null;
    /** @var ?callable */
    protected $indexCallback = null;
    protected bool $saveToSession = true;
    protected bool $sort = false;
    protected null|\Closure|string $sortBy = null;
    protected int $sortOptions = SORT_NATURAL;
    protected bool $sortDescending = true;

    protected ?Model $selection;

    /**
     * @throws \Exception
     */
    public function __construct(string $resourceClass)
    {
        if (class_exists($resourceClass)
            && is_subclass_of($resourceClass, Resource::class)
        ) {
            $this->resourceClass = $resourceClass;
            $this
                ->withLabel($this->resourceClass::singularLabel())
                ->noItemsFoundText(__('nova_toolbar_tools::nova_toolbar_resource.no_items_found', [
                    'label' => $this->resourceClass::label()
                ]))
                ->searchLabel(__('nova_toolbar_tools::nova_toolbar_resource.search'));
        } else {
            throw new \RuntimeException("'{$resourceClass}' is not a valid nova resource class!");
        }
    }

    public function saveToSession(bool $flag): self
    {
        $this->saveToSession = $flag;

        return $this;
    }

    public function selection($value): self
    {
        $model = ($value instanceof Model) ? $value : ($this->resourceClass::$model)::find($value);

        if ($model) {
            $this->selection = $model;
        }

        return $this;
    }

    public function clearable(bool $flag = true): self
    {
        $this->withMeta([
            'clearable' => $flag
        ]);

        return $this;
    }

    /**
     * Enable api-based search, takes precedence over `filterable`.
     * @param bool $value toggle the search-mode on/off
     * @return $this
     */
    public function searchable(bool $value = true): self
    {
        $this->withMeta([
            'searchable' => $value
        ]);

        return $this;
    }

    /**
     * Enable text-filter on the dropdown, `searchable` takes precedence when used together.
     * @param bool $value toggle the filter-mode on/off
     * @return $this
     */
    public function filterable(bool $value = true): self
    {
        $this->withMeta([
            'filterable' => $value
        ]);

        return $this;
    }

    public function withLabel(string $value): self
    {
        $this->withMeta([
            'label' => $value
        ]);
        return $this;
    }

    public function onChange($callback): self
    {
        if (is_callable($callback)) {
            $this->changeCallback = $callback;
        }

        return $this;
    }

    public function onDelete($callback): self
    {
        if (is_callable($callback)) {
            $this->deleteCallback = $callback;
        }

        return $this;
    }

    public function withIndexQuery($callback): self
    {
        if (is_callable($callback)) {
            $this->indexCallback = $callback;
        }

        return $this;
    }

    /**
     * Sort the results.
     *  Works like `\Illuminate\Support\Collection`.
     *  Will get ignored with `searchable`.
     *
     * @param false|\Closure|string $sortBy See `\Illuminate\Support\Collection:sortBy`. Set to false to disable soring
     * @param int $options See `\Illuminate\Support\Collection:sortBy`.
     * @param bool $descending See `\Illuminate\Support\Collection:sortBy`.
     * @return $this
     */
    public function sortBy(false|\Closure|string $sortBy, int $options = SORT_NATURAL, bool $descending = false): self
    {
        $this->sortOptions = $options;
        $this->sortDescending = $descending;

        if ($sortBy === false) {
            $this->sort = false;
            $this->sortBy = null;

            return $this;
        }

        $this->sort = true;
        $this->sortBy = $sortBy;

        return $this;
    }

    public function noItemsFoundText(string $value): self
    {
        $this->withMeta([
            'noItemsFoundText' => $value
        ]);

        return $this;
    }

    public function searchLabel(string $value): self
    {
        $this->withMeta([
            'searchLabel' => $value
        ]);

        return $this;
    }

    public function isForModel(Model|string $modelClass): bool
    {
        return (is_string($modelClass) ? app($modelClass) : $modelClass) instanceof $this->resourceClass::$model;
    }

    protected function sessionKey(): string
    {
        $resourceRouteKey = $this->resourceClass::uriKey();
        return "nova.toolbar-resources.$resourceRouteKey";
    }

    protected function getValueFromSession(): ?string
    {
        return Session::get($this->sessionKey(), null);
    }

    protected function getSelection(): ?Model
    {
        if (empty($this->selection)) {
            $this->selection = $this->getSelectionFromSession();
        }

        return $this->selection;
    }

    protected function getSelectionFromSession(): ?Model
    {
        /** @var class-string<Model> $model */
        $model = $this->resourceClass::$model;

        return $model::find($this->getValueFromSession());
    }

    protected function writeSelectionToSession($value)
    {
        Session::put($this->sessionKey(), $value);
    }

    public function handlePutRequest(NovaRequest $request): void
    {
        if ($this->saveToSession) {
            $this->writeSelectionToSession($request->get('id', null));
        }

        if (!is_null($this->changeCallback)) {
            ($this->changeCallback)($request);
        }
    }

    public function handleDeleteRequest(NovaRequest $request): void
    {
        if ($this->saveToSession) {
            $this->writeSelectionToSession(null);
        }

        if (!is_null($this->deleteCallback)) {
            ($this->deleteCallback)($request);
        }
    }

    /**
     * @param NovaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function handleIndexRequest(NovaRequest $request): JsonResponse
    {
        if ($request->has('search') &&
            strlen($request->get('search')) > 0
        ) {
            $searchResults = (new GlobalSearch(
                $request,
                [$this->resourceClass]
            ))->get();

            return response()
                ->json(Collection::make($searchResults)
                    ->map(fn ($result) => [
                        'id' => $result['resourceId'],
                        'title' => $result['title'],
                        'subTitle' => $result['subTitle'],
                        'avatar' => $result['avatar'],
                        'rounded' => $result['rounded'],
                    ]));
        } else {
            /** @var Builder $query */
            $query = ($this->resourceClass::$model)::query();
            if (is_callable($this->indexCallback)) {
                $result = ($this->indexCallback)($query);
                if ($result instanceof Builder) {
                    $query = $result;
                }
            }

            /** @var Collection $models */
            $models = $query->get();
            $list = $models->map(function (Model $model) use ($request) {
                /** @var Resource $resource */
                $resource = (new $this->resourceClass($model));

                return [
                    'id' => $model->getKey(),
                    'title' => $resource->title(),
                    'subTitle' => transform($resource->subtitle(), function ($subtitle) {
                        return (string) $subtitle;
                    }),
                    'avatar' => $resource->resolveAvatarUrl($request),
                    'rounded' => $resource->resolveIfAvatarShouldBeRounded($request),
                ];
            });

            if ($this->sort) {
                $list = $list
                    ->sortBy(
                        $this->sortBy,
                        $this->sortOptions,
                        $this->sortDescending
                    )
                    ->values()
                    ->all();
            }

            return response()->json($list);
        }
    }

    public function jsonSerialize(): array
    {
        $selection = $this->getSelection();
        if ($selection) {
            $selection = static::withNovaRequest(function (NovaRequest $request) use ($selection) {
                /** @var \Laravel\Nova\Resource $resource */
                $resource = new $this->resourceClass($selection);
                return [
                    'id' => $selection->getKey(),
                    'title' => $resource->title(),
                    'subTitle' => transform($resource->subtitle(), function ($subtitle) {
                        return (string) $subtitle;
                    }),
                    'avatar' => $resource->resolveAvatarUrl($request),
                    'rounded' => $resource->resolveIfAvatarShouldBeRounded($request),
                ];
            });
        }

        return array_merge(
            parent::jsonSerialize(),
            [
                'resource' => $this->resourceClass::uriKey(),
                'selection' => $selection,
            ]
        );
    }

    protected static function getToolInstanceFor(string $resourceClass): ToolbarTool
    {
        $tool = static::withNovaRequest(function (NovaRequest $request) {
            return collect(ToolbarTools::availableToolbarTools($request));
        })
            ->filter(fn (ToolbarTool $tool) => $tool instanceof ToolbarResource
                && $tool->resourceClass === $resourceClass)
            ->first();

        if (empty($tool)) {
            throw new \RuntimeException("No 'ToolbarResource' instance found for '{$resourceClass}'.");
        }

        return $tool;
    }

    public static function sessionValueFor($resourceClass): ?Model
    {
        $tool = static::getToolInstanceFor($resourceClass);

        return $tool->getSelection();
    }

    public static function rawSessionValueFor($resourceClass): ?string
    {
        $tool = static::getToolInstanceFor($resourceClass);

        return $tool->getValueFromSession();
    }
}
