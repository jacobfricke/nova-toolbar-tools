<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Tools;

use Illuminate\Http\Request;

use Laravel\Nova\AuthorizedToSee;
use Laravel\Nova\Makeable;
use Laravel\Nova\Metable;
use Laravel\Nova\ProxiesCanSeeToGate;

abstract class ToolbarTool implements \JsonSerializable
{
    use AuthorizedToSee;
    use Makeable;
    use Metable;
    use ProxiesCanSeeToGate;

    public string $component = 'toolbar-tool';

    public function authorize(Request $request): bool
    {
        return $this->authorizedToSee($request);
    }

    public function boot(): void
    {
    }

    public function component(): string
    {
        return $this->component;
    }

    public function jsonSerialize(): array
    {
        return array_merge([
            'component' => $this->component(),
        ], $this->meta());
    }
}
