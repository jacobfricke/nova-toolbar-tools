<?php

namespace Gabelbart\Laravel\Nova\ToolbarTools\Tools\Traits;

use Laravel\Nova\Http\Requests\NovaRequest;

trait UsesNovaRequest
{
    protected static function withNovaRequest(callable $callback): mixed
    {
        $forgetRequestInstance = !(app()->resolved(NovaRequest::class));
        $request = app(NovaRequest::class);

        $result = $callback($request);

        if ($forgetRequestInstance) {
            app()->forgetInstance(NovaRequest::class);
        }

        return $result;
    }
}
