const mix = require('laravel-mix')

require('./nova.mix')

mix
  .setPublicPath('dist')
  .js('resources/js/main.js', 'js')
  .sass('resources/sass/main.scss', 'css')
  .vue({
    version: 3,
  })
  .options({
    terser: {
      extractComments: false,
    },
  })
  .nova('gabelbart/nova-toolbar-tools')
